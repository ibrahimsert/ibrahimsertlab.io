---
title: Kör İnanç ve Korku Yerine
date: 2023-04-18
tags: ["bilim","teknoloji","zihin","bilgi","inanç",]
draft: false
showtoc: true
---
:sunny:
Önceki hasbıhâl yazımda genel bir çerçeve çizmekten öte sadece giriş yapmak istemiştim, özellikle yeni nesillerimiz için daha işlevsel bir zihin dünyası inşa etmenin önemine vurgu yaptım.  Bu ve sonraki yazılarımda bu konuyu daha uzun soluklu irdeleyip inceleme fırsatı bulacağımızı düşünüyorum. 

Hayata bakış açımızı, kararlarımızı ve eylemlerimizi belirleyen en temel unsur sahip olduğumuz bilgi ve tecrübelerdir. Bu bizi biz yapan esas etkenin de çevreyi algılama ve tecrübeye dönüştürme yetimiz olduğunu gösterir.

Çevremizde sürekli üretme, kalkınma, müreffeh yaşam, mutluluk ve güvenlik ile alakalı söylemler işitiyoruz. Daha adil ve daha zengin bir ülke, daha dindar bir toplum veya daha aydın, laik bir toplum. Sürekli birbiri ile çatıştığını ve birinin diğerine galip gelmesinin arzuladığımız sonsuz bir romantizm deryasında, yelkensiz, motorsuz ve dümensiz yol almaya çalışan alelade bir sal gibiyiz. Elbette amacım toplumsal kutuplaşmaya karşı vaaz vermek, kötülük kötüdür, iyilik de iyidir demek değil. Zaten neredeyse herkes bunu söylüyor, sanki iyiliğin iyi, kötülüğün de kötülük olduğunu çocuklar bile bilmiyormuş gibi.

Dile getirmek istediğim şey, çocuklarımıza teklif ettiğimiz gelecek, onlara teklif ettiğimiz ufuk neden bunun da ötesine geçemesin. Sadece kör inançları ve korkuları üzerinden hayatta kalmaya çalışan, üzerine giydirilmiş gömlekler ile her gün ilkeleri ve arzuları arasında çaresizce güç ve heves peşinde koşmaktan yorulmuş zihinler yerine, çevresiyle uyumlu, usta bir sanatkâr gibi hayatı ilmek ilmek dokuyan, fikri hür, vicdanı hür bir zihin miras bırakabilmek çocuklarımıza. 

Sürekli çocuklarımızdan bahsedip gelecekten dem vurmamın sebebi ukalaca bir ümitsizlik değil, çocuklar ve gelecek demek bizim öğretmen olduğumuz bir düzlemi inşa etmek demektir. Öğretmen olmak için öğrenmek gerek. Korkular ve kör inançların ötesinde hakikat arayışının taliplisi olmak, günübirlik çıkarların ve heveslerin değil, uzun vadeli hakikatin yolcusu, yolu ve rotası olmaktan bahsediyorum.

Rasyonel düşünce, bilimsel bilgi, teknolojiyi üretme ve kavrama sadece birer kelime veya halkın inancı ile çatışma için anti-tez üretme araçları olarak görülüyor. Özellikle rasyonel düşünce ve bilim birçok grup tarafından hem siyasallaştırılan hem de inançlı topluluklara karşı bir silah gibi doğrultulan, onların kin ve nefret hissetmesine, düşmanlık beslemesine ve bu işlevsel araçlardan uzaklaşmasına sebep olan enstrümanlar haline getiriliyor. Ortaya çıkan bu kutuplaşmada, sanki bir taraf dogmatik bilginin ve karanlığın temsilcisi, bir taraf da aydınlanmanın, bilimin temsilcisi gibi işlevsiz tartışma yürütülüyor.

Özellikle ülkemizde bu hususun esas göz ardı edilen kısmı, kendisini aydın, entelektüel ve bilimsel bilginin temsilcisi olarak gören grupların büyük bir kısmı kendi ideolojik yankı odalarında fark etmiyor olsalar da dindar insanları yaftaladıklarından daha  dogmatik, irrasyonel ve bilimsel olmayan bilgi ve fikirler ile dolup taşıyorlar. Toplumun büyük bir kısmını oluşturan dindar ve muhafazakâr insanların, dini inançlarının karşıtı olarak gördüğü bu kötü temsil başka tür bir bağnazlığın ürünüdür.
	
>Rasyonel düşünce ve bilimsel bilgi halkın inanç ve değerlerinin düşmanı veya karşıtı değildir. Bir otomobil veya bilgisayar ne kadar din düşmanı ise bunlar da o kadar din düşmanı olabilir. Zira bunlar sadece daha işlevsel ve verimli şekilde hayatta kalabilmemizin araçlarıdır. Hangi tarafta olursanız, toplumun hangi parçasına aidiyet hissediyorsanız hissedin, son tahlilde bu araçları hayatımıza doğru şekilde almadığımız sürece, kör inanç ve korkuları ile birbirini yiyip tüketen alelade bir üçüncü dünya ülkesi olmanın ötesine geçemeyiz. 

## 
![](https://gitlab.com/ibrahimsert/ibrahimsert.gitlab.io/-/raw/master/static/img/korinanc/korinanc.jpg)
## 


*****

Seydişehir'in Sesi Gazetesi ve İnternet Sayfasında 13.04.2023 tarihinde yayınlanan yazım.(https://www.seydisehirinsesi.com.tr/yazar/ibrahim-sert/kor-inanc-ve-korku-yerine-169-kose-yazisi)





