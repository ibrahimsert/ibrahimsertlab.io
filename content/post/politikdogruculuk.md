---
title: Politik Doğruculuk Dini
date: 2023-06-27
tags: ["fikir","politik doğruculuk","SJW","akıl","bilim","zihin","bilgi","inanç",]
draft: false
showtoc: true
---

:pray:Yazılarımda ısrarla skolastik düşüncenin her türlüsünü aşmaktan, kör inanç, korkular ve duygu sömürüsü etkisinden sıyrılıp, eylemlerimizi akıl ve mantık ile bilimsel yöntem kullanılarak belirlemekten dem vurdum.

Yazdıklarım birçok insan tarafından yanlış anlaşılabilir. İnsanların dini inançlarına yönelik eleştiri yaptığım düşünülebilir fakat benim amacım asla bu değil. Ben insanların neye, nasıl ve niçin inandıklarını değil, bu inançları vasıtası ile onların kör edilmesi, duygularının ve imanlarının sömürülmesi vasıtası ile irrasyonel(akıl dışı) davranışlara sevk edilmelerine eleştiri getirmek istiyorum. Eleştirilerim, İslam veya başka ilahi dinlere inananlar ile de sınırlı değil, Aynı zamanda, dünyada ve ülkemizde beşeri normlara bağnazca bağlı seküler toplulukların modern kabulleri, dinleştirdikleri değerleri, dayatılan sözde doğrular ve duygu sömürülerini de sırası ile dile getirmeye çalışıyorum.
	
Modern düşüncelere sahip olduğunu iddia eden, kendisini ilahi dinlerden uzak görüp, modern ve evrensel insanlık değerlerinden bahseden insanlar, ilahi dinlerin mensuplarına ve muhafazakâr halk kitlelerine göre kendilerini daha ilerici, kurtarıcı ve akılcı görüyorlar. Bu tarz fikri akımlara sahip insanlar, bilimden ve akıldan da sık sık bahsederler. Yukarıda da belirttiğim gibi sorun zaten neye inandığımız, neyi bildiğimiz değil, eylemlerimizi neyin belirlediğidir. Birilerinin akıldan, bilimden dem vurması onun gerçekten akılcı olduğu ve bilimsel yönteme sadık kaldığı anlamıyor gelmiyor maalesef. Tıpkı dindarlık kisvesine sahip bazı insanların inanç ve ahlak ilkelerini kolaylıkla göz ardı edebildiği, eğip bükebildiği veya inandığını iddia ettiği ilkelerini bireysel veya kendi zümrelerinin çıkarlarına değiştiği gibi.
## 
![](https://gitlab.com/ibrahimsert/ibrahimsert.gitlab.io/-/raw/master/static/img/politik/politik1.jpeg)
## 
	
Politik doğruculuk veya siyaseten doğruculuk, (Amerika’da SJW(Sosyal Adalet Savaşçısı)) kendisini yerel ve dini referanslardan uzak gören, batılı değerlere yakın hisseden insanların hızla benimsediği, batıdan ithal bir yaklaşımdır. Fiiliyatta, emperyalist amaçları olan güçlü ve zengin devletlerin yürüttükleri faaliyetler ve benimsedikleri politikaları maskeleme işlevi görmektedir. Bu güçler kendi halkını ve hedef aldığı coğrafyadaki halkları, halkların dini ve siyasi liderlerini teslim alarak yönetebilmektedirler. Bu kitlelerin dışında kalan grupları; evrensel değerler, etik, çevre sorunları, azınlık hakları, feminizm, çocuk hakları ve hayvan hakları gibi tabulaştırdıkları meseleler üzerinden romantizm kurgulayıp, teslim alarak büyük çirkinliklerini, yerel toplumsal sorunları ön plana çıkartarak örtbas etmek istemektedirler. Ellerinde bulundurdukları, medya, sinema, moda ve benzeri araçlar ile oluşturdukları yapay kültürel ortam sayesinde, körü körüne duyarlıklılar ve kütlesel hareketler yaratabilmektedir. 
	
Özellikle coğrafyamızda, halkın çoğunluğunu akılsız, mantıksız, irrasyonel, bağnaz ve faşist olarak nitelendiren politik doğrucular, din haline getirdikleri ön kabuller ve iman ettikleri ilkeler ile gerçek sorunların üzerinden sahte tepkiler oluşturmakta, iptal kültürü ile baskının, linçin, faşizmin en büyüğünü kendileri yapabilmektedirler. Ellerinden bulundurdukları sözde kültürel üstünlüğü, duygu ve vicdan sömürüsünü, bir silah gibi kullanarak, sanat camiasını, birçok siyasetçiyi ve sivil toplum yapılarını kontrol edebilmektedir. Ne yazıktır ki, bu süreç içerisinde normal halk kitlelerini de, sürekli tekrarlarla göze soktukları argümanlar ve algı faaliyetleri ile ikna etmeyi başarabiliyorlar. Kontrol edemediklerini de linç/iptal kültürü ve boykot ile kadın, hayvan, çocuk veya herhangi bir azınlığın düşmanı gibi afişe edebilmektedirler. Sadece ülkemizde değil, Amerika ve Avrupa’da da durum böyledir. Onların amentüsü dışına çıkarak, kendi doğru bildiğini savunan bir siyasetçi, sanatçı veya iş insanı kolaylıkla anti semitist, ırkçı, faşist, hayvan veya kadın düşmanı ilan edilebilir. Gerçekte böyle olduğu için değil elbette, mesele gerçekten bir ırkçı veya kadın düşmanı olması değil, politik doğruculuk dininin amentüsüne uymaması yeterlidir.
	
Dini grupları veya milliyetçi kitleleri; farklılıkları kabullenemedikleri veya insanlara baskı uyguladıkları gerekçesiyle eleştiren politik doğrucular, kurumsal dini yapıların aforoz/tekfir olgusunun benzerini iptal(cancel) kültürü ile kullanarak kendileri aynı şeyi sözde sahiplendikleri ulvi insani değerler için yapmaktadırlar. Bunları bir komplo teorisi olarak anlatmıyorum, binlerce insanın birkaç merkezden belli amaçlar için emir alarak yönetilebileceğine inanacak kadar saf değilim, bu yüzden politik doğruculuk ve benzeri akımların teknik olarak bir din gibi olduğunu, radikal inanlarının bulunduğunu söyleyebiliriz. İnsanlara kötülük yapmak için bir araya gelmiş bilinçli bir örgütün üyeleri değiller. Tıpkı bir dinin mensupları gibiler ama onlar politik doğruculuğa iman etmişler ve hangi bölgede veya coğrafyada olurlarsa olsunlar benzer tepkileri verip, benzer saldırıları yapmaktadırlar. Samimi olup olmamaları önemli değil, neye nasıl inandıkları da önemli değil. Birçoğu politik doğruculuk dinine inandığının farkında bile değil. Önemli olan bu kör inanç, sahte korkular ve romantizm vasıtaları ile eleştirdikleri dindar veya muhafazakâr kitlelerden daha irrasyonel(akıldışı) davranış sergilemeleridir.
	
Neye inandığımız, hangi ideolojiye yakın olduğumuz kendimiz için önemli olsa da topluma karşı sorumluluğumuz, rasyonel eylemlerde bulunmak, herkesin iyiliği için akıl ve mantık ile hareket etmeye çalışmaktır. 

## 
![](https://gitlab.com/ibrahimsert/ibrahimsert.gitlab.io/-/raw/master/static/img/politik/politik.jpeg)
## 


Esasında politik doğruculuk, ezilen, dışlanmış, inancı veya etnik kökeni sebebi ile baskı ve ayrımcılık gören topluluklara karşı kullanılan dilin ve var olan önyargıların düzeltilmesi, pozitif ayrımcılık ile birlikte onlara sosyal, ekonomik ve politik adaletin sağlanmasını talep eden bir yaklaşımdır. Bunun yanında çevre, iklim sorunları, doğa ve ekonomik eşitsizlikler konularında da hassasiyet ortaya koyan bir yaklaşım olarak tanımlanabiliyor. Fakat yukarıda bahsettiğim gibi, tüm bu iyilik elçiliğinin arkasında, aslından koparılmış, amacını aşmış, kötüye kullanıma açık, seküler SJW mücahitlerinin de eylemlerini meşrulaştıran retorikten başka bir şey bulunmamaktadır.

Kötü temsil, kötü niyet ve yıkım dışında toplumlara bir şey vaat etmeyen, inanç ve eylem ihtiyacını doyuramamış insan topluluklarını afyonlayan, kör inanç, korku ve baskıdan başka bir şey üretmeyen bir “din” e dönüşmüş durumdadır. İlahi dinlerin, toplumları kontrol etme, zulmü ve haksızlığı meşrulaştırma ve daha fazla güç elde etme aracı olarak kullanılabildiği gibi, politik doğruculuk dini de benzer amaçlar için kullanılmaktadır.
Diğer dinlerin en kullanışlı yanları, hakikatleri değil, hurafeleridir. Aynı durum politik doğruculuk dini için de geçerlidir. Politik doğruculuk dininin hurafelerinden bazıları şunlardır; 

## 1- Azınlığın veya güçsüzlerin her zaman haklı olması;
Olayların öncesini veya sonrasını incelemeden, haklı ve haksız sorgulaması yaptırtmadan, neden sonuç ilişkisinden bağımsız, kendi istedikleri tarafı haklı çıkarma çabasıdır. Emperyalist güçlerin işine yaramayan azınlıklar veya gerçekten zulüm altındaki halklar görmezden gelinir. Afrika’da birçok örgüt binlerce insanı öldürüp zulmederken göstermelik tepki ortaya konulur fakat Suriye veya Irak gibi yerlerde doğrudan müdahale edilip, bazı örgütler şeytanlaştırılır, bazıları ise özgürlük savaşçısı gibi lanse edilir. Bu noktada politik doğrucu gruplar kullanışlı araçlar olarak ortaya çıkıp seslerini yükseltirler. Irak ve Suriye’de IŞID’ in bu kadar şeytanlaştırılıp, PKK bağlantılı grupların temize çıkarılması, çiçek böcek gibi gösterilmelerinin sebebi budur. İyi terörist, kötü terörist yaklaşımı bu sayede ortaya çıkmaktadır. PKK’lı grupların yaptığı baskı ve katliamlar dünya medyasında yer bulmazken, Türkiye gibi sınır güvenliğini korumak zorunda kalan ülkelerin aldığı tedbirler canavarlaştırılarak gösterilir ve hem içeriden hem de dışarıdan baskı oluşturulmaya çalışılır.

Küçük bir çocuğun eline taş verip bankta oturan iki yetişkine taş attırılır, sonra bu yetişkinler çocuğu yakalayıp hesap sorunca bir anda kameralar ve politik doğrucular açığa çıkarlar, bakın küçücük çocuğu dövüyorlar diye yaygara kopartılır. Bu en temiz ve en yaygın taktiktir. Gerçekte; azınlıkların kötü niyetli çevrelerce yönlendirilmesi ve ses getirecek mağduriyetler yaşanması için ortam yaratılması sayesinde sınırsız bir kamuoyu desteği ve argüman üstünlüğü elde ederler.

Sadece etnik veya dini azınlıklar değil, eşcinsel azınlıklar da bu tuzağa düşürülürler. Eşcinsel derneklerin teşhirciliğe ve çocuk istismarına kadar varan eylemleri, sıradan bir vatandaş tarafından eleştirilse bile, homofobiklik ile suçlanır. Halkın büyük bir kesimi eşcinselliğin biyolojik ve psikolojik yanları konusunda malumat sahibi bile değilken, inançları ve örfleri temelinde pasif ve eylemsiz önyargıları olduğu halde, bilinçli ve örgütlü bir yok etme operasyonu varmış gibi algı oluşturularak, kamuoyunda canavar homofobikler ve ezilen LGBT grupları gibi algılar oluşturulmaktadır. Oluşan bu ortam sonraki süreçlerde gündelik siyasetin de konusu haline gelip, var olmayan bir toplumsal çatışma ortamı ve mağduriyetler yaratılarak, gerçek sorunların çözümü de engellenmiş olur. 

Etnik köken, inanç ve cinsel yönelim olguları üzerinden, gerçekte olduğundan daha büyük mağduriyetler uydurularak, sözde azınlıkları savunma söylemi üzerinden, sahte toplumsal karşıtlıklar ve iç çatışmalar üretilmektedir. Gerçek sorunlarımız, çözülebilecek azınlık problemleri çözümsüz hale getirilmektedir. 

## 
![](https://gitlab.com/ibrahimsert/ibrahimsert.gitlab.io/-/raw/master/static/img/politik/politik3.png)
## 
## 
![](https://gitlab.com/ibrahimsert/ibrahimsert.gitlab.io/-/raw/master/static/img/politik/politik4.png)
## 

## 2- Katliamın büyüğü, küçüğü olmaz;

Bu iddianın sahipleri, katliamların en büyüklerini yapan emperyalist devletler ve onların bilinçsiz kuklalarıdır. Dünya kamuoyuna en büyük ve en canice katliamlar olarak Ermeni ve Yahudi katliamları propagandası yapılır. İnsanların çoğu bu şekilde inandırılmıştır. Bu olaylarda en kötü tahminlerde bile 6 milyon Yahudi ve tüm iddiaları bir araya toplasak 1.5 milyon Ermeni ölmüştür. Batılı devletler tarafından da fonlanmakta olan birçok politik doğrucu örgüt, ülkemizi Ermeni katliamı iddiası ile suçlamaktadır. Tarihi gerçekliğin ne olduğu onlar için önemli değil. Bir papağan gibi emperyalist tezleri ortaya atan bu gruplar, sözde vicdani bakışlarını gerçek katliamlara, günümüzde devam etmekte olan kıyım ve sömürülere asla çevirmezler.

Belçika’nın Kongo da yaptığı kauçuk sömürüsünde 10 milyon Afrikalı ölmüştür. İngilizlerin, Hindistan nüfusunu kontrol altına almak için yaptığı kontrollü kıtlık faaliyetleri 60 milyondan fazla Hintlinin hayatına mal olmuştur. Fransa Cezayir’de milyonlarca insanı katletmemiş gibi meclisine Ermeni tasarısını getirip bize parmak sallamaya devam etmektedir. Amerika da Ermeni lobisi ve destekledikleri senatörler her siyasi krizde ülkemizin karşısına soykırım kartını çıkarıp koymaktadır. Yaptıkları büyük sömürü ve soykırımları perdeleyebilmek için medya ve film sektörünü tekellerinde tutarak, iyilerin de mecbur kalınca kötülük yapabileceğini, iyi şeyler yapabilmek için bazı acı reçetelerin uygulanabileceğini anlatırlar. Politik doğruculuk şövalyeleri de bu tuzağa düşerek, büyük katliamları görmeden, yerel toplumsal olayların, bilinçli ve örgütlü olmayan toplumlar arasındaki çatışmaların tarafı olurlar.

“En büyük kurbanlar, en büyük zorba olmaya hak kazanırlar.” Avrupa’da Yahudilere yönelik katliam ve sürgün bir gerçektir. Fakat bu olaylar dünyanın en büyük ve en acı soykırımı değildir. Politik doğrucular, sözde demokrasinin beşiği Avrupa’da Yahudi katliamı konusunda en ufak bir tarihsel çalışma veya eleştiriyi bile antisemitizm olarak damgalayıp, suçlu ilan ederler. Reklamı iyi yapıldığı için, Yahudilerin ve Yahudi devletinin her türlü zorbalığı yapmaya hakkı olduğu algısı yerleşmiştir. Yaptıkları katliamlar ve yaşattıkları acılar görmezden gelinir. Batılı sömürgeci devletlerin kendilerini demokrasi havarisi gibi göstermesine aldanmamak gerekir, onlar yeryüzünün yaşadığı en büyük acıların ve katliamların failleridirler. Kendi hatalarımızdan kendimiz ders alıp, barış ve huzur içinde yaşama erdemini kendi içimizde yaşatabiliriz. Onlara dileyecek bir özrümüz yoktur, zanlı biz değiliz.

Katliamın büyüğü küçüğü olur. Büyük katliamlar yapan ve yapmaya devam edenler, kurbanı olan milletleri katliamcı olmakla suçlayamazlar. Onların kuklası haline gelmiş politik doğrucular, SJW’ler, kanaat önderi bildikleri şahıs ve kurumları fonlayan demokrasi havarilerinin ellerindeki kanı göremezler. Onlara karşı gözleri kör, kulakları sağır, dilleri bağlanmıştır.

## 
![](https://gitlab.com/ibrahimsert/ibrahimsert.gitlab.io/-/raw/master/static/img/politik/politik5.jpeg)
## 


## 3- Azınlık haklarını koruma iddiası ile ortaya çıkmış örgütlerin gerçekten azınlıkların haklarını savunduğunu zannetmek;

Üniversite kampüslerinde, çoğunlukla seküler, orta ve üst gelir gruplarının yoğunlukta olduğu büyükşehirlerin merkezlerinde, twitter ve sözlük sitelerinde artık din ile kontrol edilemeyecek birtakım insanların, politik doğruculuk dini ile kontrol altına alındıklarını görmekteyiz. Düşünmeden, disiplinli şekilde fikir ve argüman üretmeden, çoğu zaman sadece moda olduğu için benimsedikleri üstün kültür(!), üstün fikir(!) ve kocaman yürekleri ile koro halinde yargı dağıtmaya devam etmektedirler. Birçok sinema ve tiyatro sanatçısı, şarkıcılar, bazı siyasetçiler ve fenomen isimler de bu koroya katılmak zorundadır. Katılmadıkları zaman başlarına neler geleceğinin de çok iyi bilirler. Okuyup araştırmamakla, aklını kiraya vermekle eleştirdikleri dindar ve muhafazakâr kitlelerden daha bağnaz ve kör olduklarının fakında bile değiller.

Bir terör eylemi yapıldığında önce hangi terör örgütünün yaptığına bakarlar. Politik doğruculuk dininin olumladığı, sözde azınlık hakları için kurulan örgütlerin iyi teröristleri ise kaç masum insanın öldüğü, ne kadar zarar verdiği önemsiz olur. Bu eylemleri haklı çıkarmak için “ama” ile kurulan cümleler arka arkaya sıralanır. “… ama devlet baskı yaptı, ama insanların bağımsızlığı, ama onlar DAEŞ ile mücadele ediyor” benzeri bir sürü retorik. Hukukun üstünlüğü gereği bile terörist olarak göremedikleri örgütlere verdikleri krediyi, kendi devletleri ve insanları için vermezler. 

Düşünmek, tahlil etmek ve temel insani ilkeler üzerinden değerlendirmek yerine, iyi terörist, kötü terörist ayrımı yapılmaktadır. Hâlbuki bu örgütler istismar ettikleri azınlık grupların hayatına katkı sağlayacak, kazanım elde edecek hiçbir şey yapmamaktadırlar. Problemleri çözmek yerine, emperyalist güçlerin istediği şekilde çözümsüz hale getirmek, izole dezavantajlı kitleleri kışkırtmak ve mağduriyet yaşamalarına sebep olacak eylemler içine sokup kahramanlık öyküleri yaratmak dışında hiçbir şey yapmazlar. Emperyalist güçlerin hesabına çalıştıkları aşikârdır. Batılı merkezlerde bürolar açmakta, rahatça faaliyet göstermektedirler. Birçok çatışma alanında silah ve para yardımı yanında fiilen destek verildiğini sık sık görmekteyiz.

Halk için halk düşmanlığı yapmak, sözde demokratik ilkeler adına en antidemokratik yöntemler ile kendileri gibi düşünmeyen insanları linç etmek en yaygın eylemleridir. Farklı bir düşünce ile cevap verildiği zaman, faşist, ırkçı, dinci veya iktidar yalakası olarak yaftalarlar. Boşuna politik doğruculuk dini demiyorum, ilahi dinlerin takipçisi kitlelerin kâfir, zındık, mülhit, mezhepsiz yaftalarına ne kadar da benzemektedir.

## 
![](https://gitlab.com/ibrahimsert/ibrahimsert.gitlab.io/-/raw/master/static/img/politik/politik6.jpeg)
## 


## 4- Milliyetçiliğin ırkçılık olduğunu iddia etmek;

Özellikle batı dünyasında yükselen sağ siyaset, ırkçılık ve şiddet dalgaları ile birlikte, yabancı düşmanlığına, ırkçılığa, hoşgörüsüzlüğe karşı yükseltilen haklı tepkilerin yanında politik doğrucular tarafından, bu kötücül unsurlar ile birlikte milliyetçilik de sık sık anılmaya ve eşdeğer tutulmaya başlandı. Topluma ve inananlarına sürekli milliyetçiliğin ırkçılık olduğu telkin ediliyor. Klasik bir karalama yöntemi olarak, faşizme, cinsiyetçiliğe, ırkçılığa ve milliyetçiliğe karşıyız sloganı atılmaktadır.

Oysa milliyetçilik, ırkçılığın tam tersidir. Irk, din, mezhep farklılıkları güdülmeksizin, bir ülkede yaşayan tüm insanlar ve halklar o ülkenin milletidir. Yaşadığımız çağda ve coğrafyada bölgecilik ve milliyetçilik bir tercih değil bir mecburiyettir. Öncelikle ailemiz, akrabalarımız, sokağımız, şehrimiz, bölgemiz için pozitif değerler üretip, iyi olmalarını sağlamaktır. Elde edilen birikim ile ayakta durarak bu sınırları genişletip tüm insanlığa katkı sağlayabiliriz. Anayasal güvence ve hukukun üstünlüğünü temin ederek ve eşit yurttaşlar olarak bir ülkede yaşamak, öncelikle kendi kendimize yeterek ayakta kalmak, sonrasında komşularımız ile sosyal ve ekonomik münasebetler kurarak eşitler arasında işbirliği sağlamak hepimizin faydasına olacaktır.

Milliyetçilik ile büyüyüp zenginleşen batı dünyasının beslediği politik doğruculuk dini, inananlarına rüya satmakta ve milliyetçiliği ırkçılık sayarak lanetlemektedir. Sınırlar kalksın, silahlar ve ordular olmasın, tüm insanlık bir olsun tarzında ütopyalar ile bir milletin moral ve disiplinin alt üst etmekle ve parçalamak ile görevli olduklarının farkında bile değildirler. Sınırlar kalksın, tüm dünya bir olsun, herkes istediği yere girip çıkabilsin vaadi, politik doğruculuk dininin cennet vaadidir.

Evrimsel arka planımız gereğince türümüzün en önemli becerisi sosyalleşme ve organize olabilmektir. Bu sayede pençemiz, kürkümüz, parçalayıcı dişlerimiz olmadığı halde canlılar arasında en zirvedeyiz. Güçlü ve diri kalabilmek için ortak değerler ve sınırlar içerisinde organize olarak ve önce kendimize, yakınımıza ve milletimize faydalı olarak hayatta kalabiliriz. Daha fazlasını yapabildiğimiz zaman coğrafyamızı aşarak daha fazla insana değer ve fayda taşıyabiliriz. 

Milliyetçilik ırkçılıktır diyen politik doğrucular, en büyük ırkçılığı kendileri yapmaktadırlar. İnsanları ırklara ve mezheplere en fazla bölenler onlardır. En ufak bir azınlığı bile kışkırtan, çatışmaları çözmek yerine ateşe benzin döken onlardır. Birlikte daha güçlü olabilecek, farklı etnik veya dini kökene sahip oldukları halde millet olabilmiş, bunun için bedel ödemiş toplulukları ırklara ayıran, parçalayanlar onlardır.

Belki de milliyetçilik, ırkçılığın çözümü ve ilacıdır. 


## 
![](https://gitlab.com/ibrahimsert/ibrahimsert.gitlab.io/-/raw/master/static/img/politik/politik7.jpeg)
## 
*****

Seydişehir'in Sesi Gazetesi ve İnternet Sayfasında 11.05.2023 - 08.06.2023 tarihlerinde yayınlanan yazım.





