---
title: Babam Için - In the Name of the Father
date: 2022-12-18
tags: ["film","inceleme"]
draft: false
showtoc: true
---

**Filmin adını duyunca aklınızda oluşan senaryoları bir kenara bırakın, izlemeye başlayınca, senaryonun çok daha farklı olduğunu göreceksiniz ve kopamayacaksınız.**


> Özgürlük, direniş, serserilik, adalet, savunma, devlet, asker, politika, gösteriler, terör eylemi, polis ve işkence.... ve mahkeme salonu, cezaevi, idealizm, baba sevgisi...

> İbdb puanı 8.1, 1993 yapımı.



![](https://gitlab.com/ibrahimsert/ibrahimsert.gitlab.io/-/raw/master/static/img/inthename/1.jpg)


İRA \[**İrlanda Cumhuriyet Ordusu** ya da kısaca **IRA** (İngilizce adı olan _Irish Republican Army_nin baş harflerinden)\] İngiltere'de bir yerleri patlatır, suç bizim elemanın üstüne kalır ve sonra olaylar gelişir.  

Dünyada birçok farklı ideoloji ve dini örgütün, direniş gruplarının teröristlik ve özgürlük savaşçılığı arasında bakış açısına göre değişen durumu üstünden, müesses devlet kurumlarının bunlara karşı mücadeleleri, kamu güvenliği, terörle mücadele konuları hakkında özlü bir eleştirisi bulunuyor bana göre.


**_Ayrıca Daniel Day-Lewis'in döktürdüğü filmdir._**




![](https://gitlab.com/ibrahimsert/ibrahimsert.gitlab.io/-/raw/master/static/img/inthename/2.jpg)

![](https://gitlab.com/ibrahimsert/ibrahimsert.gitlab.io/-/raw/master/static/img/inthename/3.jpg)



