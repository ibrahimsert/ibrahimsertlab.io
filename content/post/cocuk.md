---
title: bu koridorda bir çocuk öldü bayım
date: 2023-07-03
tags: ["çocuk","şiir","trajedi","açlık"]
draft: false
showtoc: true
---

:anger:bu koridorda bir çocuk öldü bayım

ah, çok rahatsız ediciydi

bu dünyanın gerçekleri var

bu kadar romantik ölemezsin

açlıktan ölemezsin

evet bayım, reel politik yavşaklığımızı

sessiz ve bencil oluşumuzu

zayıf bedenli bir veled

açığa çıkarmamalı

bu kadar romantik ölemezsin

açlıktan ölemezsin


bu koridorlar nice aşk şarkılarıyla inledi bayım

bir şairin dizelerinden

bir çocuğun ölümünden daha önemli şeyler var

diplomatik dengeler

anti emperyalist soslu söylemler

yayın evleri, dergiler, fikirler

daha dikkatli olmalısın veled

bu kadar romantik ölemezsin

açlıktan ölemezsin


evet bayım, faiz bir dünya gerçeği

bunca mücahidin, müteahhitliği pornoğrafik

bütüm ölümler trajedik

bu veledin yaptığı düpedüz hainliktir

kuranı anlayıp geliyoruz

gül bahçeleri imar edeceğiz sana

sabret

cennette ekmek var

daha dikkatli olmalısın veled

bu kadar romantik ölemezsin

açlıktan ölemezsin





