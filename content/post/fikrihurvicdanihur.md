---
title: Fikri Hür, Vicdanı Hür
date: 2023-05-05
tags: ["Atatürk","fikir","akıl","bilim","zihin","bilgi","inanç",]
draft: false
showtoc: true
---
:bulb:
> **“Öğretmenler, Cumhuriyet sizden fikri hür, vicdanı hür, irfanı hür nesiller ister”**
> _Mustafa Kemal Atatürk (Hâkimiyet-i Milliye: 26.08.1924)_




:seedling: Gazi Mustafa Kemal 25 Ağustos 1924 tarihinde Ankara’da toplanan Muallimler Birliği Kongresi’nde öğretmenlere hitaben böyle sesleniyor. O, rasyonel düşünceye büyük önem veren bir liderdi. Düşünce dünyasının temelinde bilimin ve akıl yürütmenin çok büyük yeri vardı. Öğretmenlerden beklentisi; fikri hür, vicdanı hür nesiller yetiştirmeleriydi.  Geçen bunca yılın ardından bu konuda büyük bir başarı elde edemediğimiz ortada. İşaret edilen bu mefkûre bilinçli veya bilinçsiz birçok etken yüzünden ne tam olarak anlaşılabilmiş, ne de başarıya ulaşabilmiştir.

Ailemizden, çevremizden ve eğitim hayatımızdan edindiğimiz fikir, inanç ve korkular benliğimizi oluşturan, irademizi belirleyen en önemli etkenlerdir. Karşıt gördüğümüz grupları ve insanları anlayabilmek, sağlıklı iletişim kanalları yaratmak yerine, alınganlık, düşmanlık, öfke ve korku duyguları ile zihnimizi esir edip, bir arada yaşama, hoşgörü ve empati yeteneklerimizi törpülemekteyiz.
 
Zihnimizi ele geçiren kör inanç ve korkular, düşmanlık ve öfke yerine, anlamak, öğrenmek, analiz etmek, deney ve gözlem ile zihinsel kabiliyetimizi kullanmak, fikri hür vicdanı hür olmanın ilk adımıdır. Daha önceki yazımda dile getirdiğim gibi kör inanç ve korku mefhumu sadece toplumun dindar ve muhafazakâr kesimleri için geçerli olan ve yalnızca onlara yöneltilebilecek bir suçlama da değildir. Ülkemizde kendisini batılı, laik veya Atatürkçü olarak ifade eden kitlelerin de bir çok meseleyi inanç haline getirdikleri, bazı düşüncelere bağnazca saplanıp kaldıkları ve korku dürtüsü ile hareket ettikleri ortadadır.

Uygar dünya, gelişmiş devletler olarak bilinen Avrupa devletlerinde modern çağın karın ağrısı sayılabilecek; sözde çevrecilik, cinsiyet hakları, hayvan hakları(!) ve benzeri birçok konuda politik doğruculuk dininin, SJW (Social Justice Warrior) muhafazakârlığının pençesine düşmüş oldukları görülebilmektedir. Özgürlüğün ve adaletin beşiği olmakla övünen Avrupalı;  tabu haline getirdiği sözde uygar değerlerin tartışmaya açılmasına bile tahammül edememekte, linç edip, hedef göstermektedir. Avrupalı düşünce gruplarının birçoğunun, çarpık inançları sebebiyle bir kadını taşlayarak öldüren Afganistanlı dinci güruhlardan teknik olarak çok farklı olmadıkları görülebilmektedir.

Son tahlilde hangi inanca veya fikre sahip olduğumuzun önemi yok. Problemin temelinde, eylemlerimizi belirleyen esas unsurun duygusallık ve romantizm kaynaklı irrasyonel düşünce olmasıdır. Sözde modern değerlere körüne fanatiklik yapmak ile dinsel/ geleneksel fikir ve duyguların fanatizmi arasında çok bir fark yok.

Gazi Mustafa Kemal’in bu sözü söylerken beklentisi tam olarak neydi bilmiyorum. Fakat benim şahsi gözlemlerim neticesinde vardığım nokta; sadece duygularımız, fantezilerimiz ve korkularımız ile değil; gerçekçi, pragmatist(faydacı) beklentiler içerisinde olmak ve bilimsel bilginin, istatistiksel verilerin, gözlemlerin neticesinde beklentiler oluşturma ve bu yönde eylemlerde bulunmak üzerinedir.
 
Vicdan’ı duygusal yüklerden, mensubiyet duyduğu zümrelere değil, tüm toplumun hatta tüm insanlığın hayrına sorumluluk hissedecek, fikri nesnel verileri temel alıp ortak iyinin çıkarları için çalışacak, gerçek anlamda fikri hür, vicdanı hür bireyler olmak ve bireyler yetiştirmek,  peşinde koşmaya değer en faydalı yaklaşım olacaktır.



## 
![](https://gitlab.com/ibrahimsert/ibrahimsert.gitlab.io/-/raw/master/static/img/fikrihur/fikrihur.jpeg)
## 


*****

Seydişehir'in Sesi Gazetesi ve İnternet Sayfasında 27.04.2023 tarihinde yayınlanan yazım.(https://www.seydisehirinsesi.com.tr/yazar/ibrahim-sert/fikri-hur-vicdani-hur-180-kose-yazisi)





