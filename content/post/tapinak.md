---
title: Aşk Tapınağının Minaresinde
date: 2023-10-05
tags: ["fikir","akıl","bilim","zihin","bilgi","inanç"]
draft: false
showtoc: true
---

 :sparkles:Dünyaya romantik gözlerle bakan, irrasyonel ve hayalci birisi değilim. Hayatta olmayı, basit şeylerden keyif almayı seviyorum. Güzel bir bahar gününde, yolun kenarındaki çimenlik ve ağaçlı kısımda ağır ağır yürümenin, bir gökyüzüne, bir ağaçlara, biraz da yeşilliğe göz atarak düşüncelere dalmanın verdiği huzur bile yaşamayı güzelleştirmeye yeter. Fakat bu dünyaya ait değilim. Göçebe olduğumu biliyorum, buranın yerlisi değilim. Bedenim ısrarla burada çakılı kalmak isterken, içimden bir parça “Tebarakellahü ahsenül halikin” diye sayıklayarak, durma yürü, sen buralı değilsin, biraz ekmek ye, biraz da su iç, devam et yoluna diyor.

 İnsanın iki adet doğası olduğunu düşünüyorum, hayvan ve ruh. Bu biyolojik makine uygun hava yakıt karışımı ile kükreyen içten yanmalı bir motor gibi, sürekli daha fazlasını istiyor. Tatmin etmek, içgüdüsel/hormonsal isteklerini dizginlemeden başa çıkmak mümkün değil. Hayvani isteklerimizin ötesinde, bizi Âdem yapacak olan, dizginlere sımsıkı asılıp, gerçekten güzel olana yönlendirecek olan Hak’tan bir cüz olan en değerli parçamız ruhumuzdur. 

 Bu dünyanın peşinde, koşmaktan çatlamış atlar gibi dört nala yol alıyoruz. Her zaafımızın bir bahanesi var, her günahın bir azmettiricisi. Peki ya ruhumuz ne alemde? Amacım vaaz vermek değil, din anlatmıyorum, onu yapan çok. Vaaza değil hakikate ihtiyacımız var. Bilmediğimiz için değil, hırsımızdan, arzularımızdan perişan olmuşuz.

 Gelecek kaygısı, geçmişin pişmanlıklar, arzular, açlık her şeye karşı duyduğumuz sonsuz açlık… Hiç bitmek tükenmek bilmeyen bir susuzluk. Sahip oldukça daha da korktuğumuz, daha fazlasını elde ederek üzerine örtmeye çalıştığımız bir huzursuzluk.

 Sürekli tekrar edip durduğumuz dindarlığımız ve kendimizi temize çıkardığımız ahlaki duruşumuz ile adaleti, paylaşmayı, kanaat etmeyi unutmuşuz. Ruhumuzun bedenimize ne kadar hâkim olduğunu biliyor muyuz?

 Şükür ve şevk deryasını, bağ ve bahçeye değiştik, altın toplamak için gerçek hazineden vazgeçtik. Kendimize saraylar inşa etmek için, iyilikler tanrısının sonsuz iklimlerindeki saltanatı kaybettik. Masa başında böbürlenmek için, aşk tapınağının minaresinde gururumuzu çiğnemeyi unuttuk. 

 Adımızı suya vermek varken, ekmeğe sattık. Lezzet ve zevk içinde, gam ve kederden korkar olduk. Yaşama bağlanmak yerine, yaşayanlara bağlandık. Gerçekle kutsanmak varken, yalanla övülüyoruz. Evleri mihraba, yükselmeyi Miraç’a tercih ettik. Göklerde uçmak varken, yeryüzünde sürünüyoruz. 

 Sanatçı olmalı, yaratıcı olmalı ki nübüvvet ve risâlet bulup ebedileşmeli. 

 Elimizdeki güç ve zenginlik ile dalkavukluk ve yağcılık elde edilir ama hakikat yolcusunu ancak asla dünyaya teveccüh etmeyen bir kalp över. Dünya esasında bir çöplüktür, bu kalpte güzellikten, imandan ve sevgiden başka bir şey bulunmaz. 

 Peki ya mumu söndürdüğümüzde ışığı nereye gider?



**Esen kalın…**

*****

Seydişehir'in Sesi Gazetesi ve İnternet Sayfasında 05.10.2023 tarihinde yayınlanan yazım.(http://www.seydisehirinsesi.com.tr/yazar/ibrahim-sert/ask-tapinaginin-minaresinde-314-kose-yazisi)







