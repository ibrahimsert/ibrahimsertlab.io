---
title: WOLFIED
date: 2022-12-17
tags: ["bilgisayar","dos","oyun"]
draft: false
---

## Eskiden İnternet yokmuş, MS-DOS varmış, bir de Wolfied ...

***

#### Malumunuz, eskiden İnternet her evde yoktu, bu kadar yaygın ve ulaşılabilir değildi, yaşım küçük olmasına rağmen ben bile bilgisayar ile tanıştıktan yıllar sonra İnternet geldi evimize..

![ms_dos](https://gitlab.com/ibrahimsert/ibrahimsert.gitlab.io/-/raw/master/static/img/wolfied/ms_dos_komut_sistemi_5472_0.jpg)

 Tabii ki işletim sistemleri de şimdiki görselliğin, kullanım kolaylığının ve alışkanlıklarımızın çok gerisindeydi. Şimdi İnternetsiz bilgisayar ile vakit geçiremeyen bizlerin yerinde, o günlerde bir fare imleci bile olmayan bir sürü insan, satırlar arasında saatlerini harcıyor, gerçek birer bilgisayar kullanıcısı oluyorlardı.  

O günleri yaşayanların gözdesi olan, birkaç program ve oyun var bildiğim kadarıyla, işte Wolfied de onlardan birisi.Az önce bir video paylaşım sitesinde, o günlerde basit kopyalama, taşıma, silme işlemlerinde kullanılan Norton Commander isimli programın nostalji görüntülerini izlerken, klasörler arasında gördüm Wolfied’i :). Ben  o günlerde bilgisayar kullanamayacak kadar küçüktüm belki ama, yıllar sonra babamın sayesinde tanıma ve saatlerce belki günlerce oynama fırsatım olmuştu. Kısa bir araştırma ile oyunu buldum tekrar.

![](https://gitlab.com/ibrahimsert/ibrahimsert.gitlab.io/-/raw/master/static/img/wolfied/Screenshot-5.png)

Oyunu açıp bakan yeni nesilden bir çok kişi belki burun kıvıracaktır, bununla saatlerce nasıl oynanır diye ama inanın bana, İnternet ve yüksek kalite grafiklere sahip oyunlarınız olmasa, binlerce mp3 depolayacak, hd filmler izleyebileceğiniz multimedya imkanlarınız olmasa, bal gibi de oynarsınız.

***

Bir de tank oyunu vardı, worms oyununa benzer ama çok daha basit grafiklere sahip, böyle karşılıklı oynayabiliyorduk, ismini bir türlü hatırlayamıyorum. Bulursam yeniden oynamak istiyorum.

![](https://gitlab.com/ibrahimsert/ibrahimsert.gitlab.io/-/raw/master/static/img/wolfied/tankwars.gif)



## [WOLFIED OYNA](https://href.li/?http://playdosgamesonline.com/volfied.html)



