---
title: “Credo quia absurdum.” İnanıyorum çünkü saçma.
date: 2023-06-25
tags: ["fikir","akıl","bilim","zihin","bilgi","inanç",]
draft: false
showtoc: true
---

:page_with_curl:“Credo quia absurdum.”:page_facing_up: İnanıyorum çünkü saçma, diyor Tertullian. Belki anlayamıyorum o halde tanrısaldır demek istedi. Belki de, insan aklının anlamlandıramadığı şeylere, tam olarak öyle oldukları için inanıyoruz. Zaten ölümün kaçınılmaz oluşu hayatımızı absürtleştiriyor. Ölüm hakikatine rağmen hırsla yaşama tutunup, sevmeye, biriktirmeye, ele geçirmeye ve inanmaya devam ediyoruz, bu garip durumu kabul ediyoruz.

Tüm bu gariplikler içerisinde inancın ispatı peşinden koşmak, akli deliller ile ilahi mesajı kanıtlama gayretine düşmek, ölümlü dünyaya mal mülk biriktirmekten farksız. Akıl ile inancın keşfedileceğini zannetmek tıpkı ölümü unutmak gibi. İnsan ölümü unutmasa, nasıl ölmeyecek gibi yaşayabilir ki? İnanmak acı ve emek isteyen, ruhani bir yolculuktur. Dünya üzerinde çeşitli dinlere mensup milyarlarca imanlı insan olmasına rağmen, imanı için ruhsal mücadeleye girmiş, emek vermiş, bedel ödemiş insan sayısı çok azdır. Bu insanlar sadece imanı bilmektedirler, iman etmek başka bir evre, bambaşka bir haldir.

Bana; akıl ve delil ile iman etmek iddiası yerine, aklı aşıp iman etme durumu daha isabetli geliyor. İnanmak ile bilmek arasındaki farkı keşfedememek, ampirik bilgi ile dinsel bilginin kullanım alanlarını belirleme hususunda problemlere yol açıyor. Dünyanın yuvarlak olmadığı, Güneş’in Dünya etrafında döndüğüne dair bilgiler, yüzyıllar boyunca dinlerin inanç esaslarından görüldü, bilgi değişti fakat itikatlar ortada kaldı. Elbette dini (dogmatik) bilgiler içerisinde, gerçeklik olarak algıladığımız fiziki dünya hakkında birçok bilgi bulunmaktadır. Fakat bu bilgileri deney ve gözlem ile elde ettiğimiz tecrübeye dayalı bilgiler gibi değerlendirmek, ampirik bilgi değişince inancımızı yalanlıyormuş gibi algılanabilmektedir. Dini bilgi bize astronomi, tarih veya fizik öğretmeyi değil, kıssalar, tarifler veya doğrudan mesajlar ile ahlaki ilkeleri benimsetmeyi, iman yolunda girişeceğimiz ruhani mücadelenin rehberliğini yapmayı amaçlar. 

Bu noktadan bakıldığı zaman, bilimsel bilginin ürettiği veriler din dışı veya dine karşı olamazlar. Akıl ve bilim imanın düşmanı veya dostu değildir. Bilimsel bilgi, yanlışlanabilirlik ilkesine dayandığı için sürekli değişebilir; İnsanlar için işlevseldir, problemlerini çözer, yeni problemler üretirse de zaman içinde onları da çözmektedir. Belki insanların ve bu dünyanın sonunu getirecek, belki de daha güzel bir yer yapacak, bunu zamanla göreceğiz. Hayatımızı kökünden değiştiren bilim ve teknolojinin bu büyük gücü,  bir tek inanca karşı koyamaz. 
İnancımızı kaybetme korkusu, siyasal, kültürel düşüş ve özgüven eksikliği ile doğrudan bağlantılı. İslam dünyası kültürel ve siyasal üstünlüğünün zirvelerinde iken, müslüman âlimler; felsefe, astronomi, matematik, tıp, biyoloji ve fizik alanlarında çağlarının ötesinde çalışmalar yaptılar. Büyük bir kısmı, ampirik bakış açısı ve rasyonel düşüne ile bilgi üretme, hayatı kavramak konusunda komplekse sahip değillerdi. İnançları zaten üstündü ve bu dünyayı keşfetme, anlama ve anlatma serüvenlerinde inançları ile çelişki hissetmediler.

Günümüzde dindar kitlelerin, akılcılık, bilim ve felsefeye bu kadar şüpheli yaklaşmalarının, bunlardan korkmalarının sebebi zayıflık, kör inanç ve yersiz korkudur. İçinde bulunduğumuz ruh hali ve bu yaklaşım, karanlık çağ diye bilinen Orta Çağ’dan daha kötü durumdadır. Orta Çağ da Tirivium ve Quadrivium okutulmadan ilahiyat eğitimi verilmiyordu. Bu olgu İslam’ın ilim merkezlerinden Semerkant’ta da böyleydi, hristiyan ilahiyatı eğitim merkezlerinde de böyleydi. Tirivium ve Quadrivium 3’lü ve 4’lü demek. İçerisinde musiki (riyaziyat), aritmetik, astronomi, matematik, belagat ve mantık var. Bunlar okutulmadan ilahiyat eğitimine başlanmıyor. Şimdi bıraktık matematiği veya geometriyi, ilahiyat eğitiminden felsefe ve kelam derslerini çıkarmaya çalışıyoruz.  

Akıldan korkmak sonucu değiştirmeyecek. İçinde bulunduğu duruma ikna olmuş, kör inanca saplanıp kalmış ihtiyarlar, çocuklarımız ve torunlarımız deist veya ateist oluyor diye çırpınıyorlar. Çareyi çocuklarına kör inanç aşılamakta buluyorlar. Hak yoldan sapmış, kafası karışık görülen gençlerin büyük bir kısmı, belki de hazırda bulduklarını değil, emek ve acı vererek hakkıyla iman edebilmenin yolunda, ruhani yolculuklarını yapmaktadırlar. Onların ulaşacakları nokta, yürüdükleri yol, hazırda bulduklarından daha kıymetli ve isabetli olabilir. 

Tekrar vurgulamakta fayda görüyorum; Allah’a hakkıyla iman etmek acı ve emek isteyen ve asla bitmeyen meşakkatli bir yolculuktur. Atalarımızdan hazır bulduklarımız ile bu gemiyi yüzdürmeye devam edemeyiz. Korku ile daha fazla insanı bu gemiden atmaya çalışmak da çözüm değil. Bu geminin motoru hakiki iman ise, gövdesini sağlam tutacak olan akıl ve bilimsel bilgidir. Çatışma bu gemiyi batıracaktır. 

## 
![](https://gitlab.com/ibrahimsert/ibrahimsert.gitlab.io/-/raw/master/static/img/absurd/absurd.jpeg)
## 


*****

Seydişehir'in Sesi Gazetesi ve İnternet Sayfasında 04.05.2023 tarihinde yayınlanan yazım.(https://www.seydisehirinsesi.com.tr/yazar/ibrahim-sert/credo-quia-absurdum-inaniyorum-cunku-sacma-185-kose-yazisi)





