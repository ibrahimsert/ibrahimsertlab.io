---
title: Teknoloji, Bilim ve Bilgi Üzerine Hasbıhâl
date: 2023-04-17
tags: ["bilim","teknoloji","zihin","bilgi",]
draft: false
showtoc: true
---

Gündelik siyasetin doruklarına kadar hissedildiği, belki haklı gerekçeler ile her anımızı işgal ettiği şu günlerde, daha uzun soluklu bir dimağ inşası göz ardı edilirken, elimizde dev ekranlı oyuncaklar, sabahın ilk ışıkları ile dopamin salgımızı hızla tüketip, her günün akşamında daha mutsuz insanlar olurken, başka şeyler söyleyebilir miyiz diye sesli düşünmeye geldim.
## 
![](https://gitlab.com/ibrahimsert/ibrahimsert.gitlab.io/-/raw/master/static/img/teknobiluzerine/3.png)
## 
Bilgi ve teknolojinin eskiden yıllar süren atılımları, artık saatler almaya başladı. Deneysel çalışmaların ürünü olan yapay zeka tasarımları, henüz bitmiş bir ürün bile değilken doğru **istem mühendisliği**(Prompt Engineering-sufle mühendiliği belki) denilen, yapay zekaya doğru istemlerde bulunup, en ideal çıktıyı alma işi bir anda birçok şirkette mesleğe dönüştü. Hala servetler harcayıp çocuğumuz alelade bir mühendislik okusun diye çabalarken, bizi neler beklediğine dair fazlaca fikrimiz olduğunu sanmıyorum. Bu sadece bir örnek elbette, fakat kesin olan şu ki; dedelerimizin, babalarımızın ve bizim uyum sağlama becerimiz ile başat giden gelişim ve dönüşüm, ufkumuzun ötesine doğru hızla kaymaya başladı.

Hangi ufku inşa edeceğiz? Kastettiğim bir teknoloji öykünmeciliği değil, batı hayranlığı değil, doğrudan hayatımızı değiştiren endüstri ve ekonomi yepyeni bir sosyoloji yaratırken biz bize ait olanı, kendi gelişim ve dönüşümüzü nasıl inşa edeceğiz. Teknolojiye aşina olan bizler ve gençlerimiz büyük oranda tüketici konumunda. İçerikleri, teknoloji ve cihazları tüketiyoruz, peki neyi üretiyoruz? Kullandığımız bu oyuncakları bozmaya, sökmeye, basit de olsa benzerini yapmaya aşina edemediğimiz öğrencilerimiz de sadece tüketici olacaklar. Bu devletin eğitim reformu ile, anaokullarına kodlama temrinleri koymakla olacak bir şey değil. Bireylerin tekil başarıları, bazı şirketlerimizin elde ettiği ilerlemeler hepimizin gururu, fakat teknoloji ve bilgiyi üreten toplum olma kararlılığı, devlet desteği ile bireysel başarılar ile eğitim müfredatında reform yapmak ile olacak bir şey değil. Bu olguyu toplumsal dinamiklerimizden birisi haline getirmek, sosyolojimizin bir parçası yapmak bizi sonuca daha hızlı götürecektir.

Tüm bu değişimin temelinde insanoğlunun dünyaya bakışı ve bilgi üretiminde ulaştığı standart yer alıyor. Son birkaç yüzyıl içerisinde binlerce yıllık insan yaşantısı hızla değişti. Bugün batıdan geldiği için, sahibi olmadığımız, ithal ettiğimizi düşündüğümüz tüm bu gelişim, belki de bizim yitik malımızdı. Avrupalılar çamur ve bataklılar içerisinde birbirlerini gırtlaklarken, bizim alimlerimiz göğe baktılar, soru sordular, deneyler ve gözlemler yaptılar. Malımızı geri alalım, çocuklarımız merak etmeyi ve soru sormayı öğrensin, sadece bunu öğrensinler. Bilgiden ve özgürlükten korkarsak, cahil ve köle olarak kalırız.

Bilim felsefesi öğretmediğimiz çocuklarımız, bilimsel bilgi ile dogma arasında farkı idrak edemeyen anne babalara dönüşecekler. Yanlışlanabilirlik ilkesinin bilgiyi üretme ve işleme alanında ne kadar işlevsel olduğunun farkında olmasak da, bir hastane kapısından içeriye girip çare aradığımızda, evimize internet bağlattığımızda veya araçlarımızın motorlarının çalışma prensibinde o hep bizimle.

Popüler bilim yayınları ilgimizi pek çekmiyor, satılmayan ürün de üretilmiyor haliyle. Deprem, iklim, sağlık ve ekonomide konu ne olursa olsun milyonlarca insanı bir şekilde hayatta tutan şey bilim ve teknoloji artık. Bu yeni paradigma hoşumuza gitmiyor olabilir, çıktılardan hoşlanmıyorsak bize ait olanı üretmemiz gerekiyor. Ortodoks yaklaşımlardan sıkıldıysak kendi bilgi ve teknolojimizi kendi kültürümüz içerisinde yeniden üretmek zorundayız.

>Çok daha spesifik bir konu olan, merkeziyetsiz internet, federal sosyal ağlar ve açık kaynak felsefesi konularından bahsetmeyi planlıyordum, fakat biraz düşününce, öncelikle daha derinden bir hasbıhâl ile başlamak gerekiyor dedim. Sonraki yazıda sesli düşünmek üzere esen kalın.

## 
![](https://gitlab.com/ibrahimsert/ibrahimsert.gitlab.io/-/raw/master/static/img/teknobiluzerine/1.png)

##
*****
[Seydişehir'in Sesi Gazetesi ve İnternet Sayfasında 07.04.2023 tarihinde yayınlanan yazım.](http://www.seydisehirinsesi.com.tr/yazar/ibrahim-sert/teknoloji-bilim-ve-bilgi-uzerine-hasbihal-164-kose-yazisi)




